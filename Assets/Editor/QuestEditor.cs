﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(QuestionGenerate))]
public class QuestEditor : Editor
{
    private string[] strings = {"aaa","aaa","aaa" };

    public override void OnInspectorGUI()
    {
        EditorGUILayout.HelpBox("Kérdést hozzátudsz adni az alsó 'Adder' résznél",MessageType.Info);
        DrawDefaultInspector();

        QuestionGenerate myScript = (QuestionGenerate)target;

        GUILayout.BeginVertical();
        GUILayout.Space(10);
        
        if (GUILayout.Button("Add New Question\nQuestions: " + myScript.questions.Count))
        {
            if (myScript.adderQuestion.QuestionString.Length > 4 && myScript.adderQuestion.CorrectAnswer.Length > 2 && myScript.adderQuestion.failAnswers.Length == 3)
            {
                myScript.questions.Add(myScript.adderQuestion);
                GameObject.FindGameObjectWithTag("MainCamera").name = "Camera - Jelenlegi Kérdések száma:" + myScript.questions.Count;

                myScript.adderQuestion = new Question("","", strings);
                Debug.Log("Hozzáadtál egy kérdést az adatbázishoz! Count: " + myScript.questions.Count);
            }
            else
            {
                Debug.LogErrorFormat("Nincsenek kitöltve helyesen!");
            }
        }

        GUILayout.Space(10);

        if (GUILayout.Button("Remove Last Question\nQuestions: " + myScript.questions.Count))
        {
            myScript.questions.Remove(myScript.questions[myScript.questions.Count - 1]);
            Debug.Log("Kitörölted az utolsó kérdést!");
        }
        GUILayout.EndVertical();
    }
}
