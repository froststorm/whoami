﻿using UnityEngine;
using System.Collections;
using UnityEngine.Purchasing;
using System;

public class ShopManager : MonoBehaviour, IStoreListener
{
    public static ShopManager Instance { set; get; }


    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

    public static string removead = "1";

    private void Awake()
    {
        Instance = this;
    }


    void Start()
    {

        if (m_StoreController == null)
        {

            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {

        if (IsInitialized())
        {
            // init megvan
            return;
        }


        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct(removead, ProductType.NonConsumable, new IDs {
            {"1", GooglePlay.Name }
        });

        UnityPurchasing.Initialize(this , builder);
    }
    private bool IsInitialized()
    {

        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    //ezt állítsd be a remove ad vásárlás gombra
    public void RemoveadVasarol()
    {
        BuyProductID(removead);
    }


    void BuyProductID(string productId)
    {

        if (IsInitialized())
        {

            Product product = m_StoreController.products.WithID(productId);


            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));

                m_StoreController.InitiatePurchase(product);
            }

            else
            {

                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }

        else
        {

            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {

        Debug.Log("OnInitialized: PASS");


        m_StoreController = controller;

        m_StoreExtensionProvider = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {

        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {

        if (String.Equals(args.purchasedProduct.definition.id, removead, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

            //a játékos v felhasználó megvásárolta a remove ad terméket ide jöhet a kód
            //pl.
            PlayerPrefs.SetInt("noreklam", 1);
            //így egy egyszerű if el el tudod intézni a reklám megjelenítését a reklámmegjelenítő kódodban
            //pl.
            //  if (!PlayerPrefs.HasKey("noreklam"))
            //  {
            //    reklám megjenít
            //   }

        }



        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }


        return PurchaseProcessingResult.Complete;
    }
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {

        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}
