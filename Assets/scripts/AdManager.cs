﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour {

    public static AdManager Instance { set; get; }

    public GameObject counterPanel;

    void Awake()
    {
        Instance = this;
      //  ShowAd();
    }
    public void ShowRewardedAd()
    {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                counterPanel.SetActive(true);
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
    
    public void ShowAd()
    {
        int x = Random.Range(0,5);

        if (x < 2)
        {
            if (Advertisement.IsReady() && !PlayerPrefs.HasKey("noreklam"))
            {
                Advertisement.Show();
            }
        }
    }
}
