﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine.SceneManagement;


public class GameLogic : MonoBehaviour
{
    public static GameLogic Instance;
    public static int CriticalCoin;
    public static int Level;
    public static int questionID;

    [Header("Árak")]
    public int[] prices;
    public bool[] useSkills;

    [Header("Text Objects")]
    public Text levelText;
    public Text CCText;
    public Text SecText;
    public Text CCCounter;
    public Text HighScore;
    public Image[] answerButton;
    public Text[] arak;
    public Text highscoreSecond;

    [Header("Colors")]
    public Color correct;
    public Color fail;
    public Color showCorrectColor;

    [Header("Timer")]
    public float TimeSec;

    [Header("Background")]
    public Image backgroundIM;
    public Image backgroundPICT;
    public Sprite[] backgroundSprites;

    [Header("Egyéb dolgok")]
    public bool isPlay;
    public Image timerImage;
    public Text questionBoxText;
    public Image[] buttons;
    public Image[] skills;

    [Header("Animations")]
    public string[] LoseAnimations;
    public GameObject[] LoseObjects;
    public string[] OnAnimations;
    public GameObject AN;

    public GameObject[] mainMenuObj;
    public string[] mainMenuStrings;
    public string[] mainMenuStringsOn;


    [Header("Google Plays")]
    public string[] googlePlayIDS;
    public int[] achivementsQuestionIDS;

    public bool generating;
    public GameObject boom;

    public Text title;
    private System.Random _rnd = new System.Random();

    public GameObject gameOverPanel;
    public GameObject continuePanel;

    void Awake()
    {
        Instance = this;
    }
    void Start()
    {

        PlayGamesPlatform.Activate();

        Social.localUser.Authenticate((bool success) => {
            // handle success or failure
        });

        CriticalCoin = PlayerPrefs.GetInt("CC");

        HighScore.text = PlayerPrefs.GetInt("HI").ToString();
        CCCounter.text = PlayerPrefs.GetInt("CC").ToString();

        levelText.text = Level.ToString("0");
        CCText.text = CriticalCoin.ToString("0");
        backgroundIM.sprite = backgroundSprites[Random.Range(0, backgroundSprites.Length)];
        backgroundPICT.sprite = backgroundSprites[Random.Range(0, backgroundSprites.Length)];


        for(int x = 0; x < prices.Length;x++)
        {
            arak[x].text = prices[x].ToString("CC 0");
        }

        if (CriticalCoin < 0)
        {
           
            PlayerPrefs.SetInt("CC", 1);
            CCText.text = PlayerPrefs.GetInt("CC").ToString("0");
        }

    }
  
    public void SetColor()
    {
        answerButton[QuestionGenerate.correctID].color = correct;
        answerButton[QuestionGenerate.correctID].GetComponent<Animation>().Play("Great");

        for(int x = 0; x < answerButton.Length;x++)
        {
            if(x != QuestionGenerate.correctID)
            {
                answerButton[x].color = fail;
            }
        }
    }

    public void ShowLeaderboard()
    {
        Debug.Log("ShowLeaderboard");
        Social.ShowLeaderboardUI();
    }
    public void ShowAchievements()
    {
        Debug.Log("ShowAchievements");
        Social.ShowAchievementsUI();
    }

    public void SelectAnswer(int myId)
    {
        if (isPlay)
        {
            if (myId == QuestionGenerate.correctID)
            {
                Level++;
                if (CriticalCoin == 0)
                {
                    CriticalCoin += 10;
                }
                else
                {
                    float adder = (CriticalCoin * 0.09f) + Level;
                    CriticalCoin += (int)adder;
                }
                for (int x = 0; x < answerButton.Length; x++)
                {
                    answerButton[x].color = Color.white;
                }


                generating = true;
                TimeSec = 15;

                GameObject go = (GameObject)Instantiate(boom,new Vector3(0,-7,11.8f),Quaternion.identity);

                Destroy(go,5);
                SetColor();

                Debug.Log("Korrekt! MyID: " + myId + " | CorID: " + QuestionGenerate.correctID);

                if(questionID==achivementsQuestionIDS[0])
                {
                    Social.ReportProgress(googlePlayIDS[0], 100.0f, (bool success) => {
                        
                    });
                }
                if (questionID == achivementsQuestionIDS[1])
                {
                    Social.ReportProgress(googlePlayIDS[1], 100.0f, (bool success) => {

                    });
                }
                if (TimeSec > 6)
                {
                    PlayGamesPlatform.Instance.IncrementAchievement(googlePlayIDS[2], 1, (bool succes) => { });
                }
                
                if (Level > 50)
                {
                    PlayGamesPlatform.Instance.IncrementAchievement(googlePlayIDS[4], 1, (bool succes) => { });
                }
            }


            if (myId != QuestionGenerate.correctID)
            {
                float adder = CriticalCoin * 0.1f;
                CriticalCoin -= (int)(adder * 0.4f);

                if(CriticalCoin < 0)
                {
                    CriticalCoin = 0;
                }

                Debug.Log("Hiba!!");
                isPlay = false;

                SetColor();
                Lose();
            }

            timerImage.fillAmount = 0;

            levelText.text = Level.ToString("0");
            CCText.text = CriticalCoin.ToString("0");
      
        }
    }

    IEnumerator LoseCour()
    {

        yield return new WaitForSeconds(3);
        Lose();
    }

    void Update()
    {
        if (isPlay)
        {
            TimeSec -= Time.deltaTime;
            SecText.text = TimeSec.ToString("0 SEC");

          
            Debug.Log("Playing!");
        }
        if(TimeSec < 4)
        {
            SecText.color = fail;
        }
        else
        {
            SecText.color = Color.white;
        }
        if (TimeSec <= 0)
        {
            SetColor();
            StartCoroutine(LoseCour());
            isPlay = false;
            TimeSec = 15;
            Debug.Log("Idő lejárt!");
            
            PlayGamesPlatform.Instance.IncrementAchievement(googlePlayIDS[3], 1, (bool succes) => { });
         
        }

        if (generating && isPlay && AN != null)
        {
            timerImage.enabled = true;
            timerImage.fillAmount += Time.deltaTime / 0.3f;
            questionBoxText.text = "";

           
            if(timerImage.fillAmount >= 1)
            {

                timerImage.enabled = false;

                for (int x = 0; x < answerButton.Length; x++)
                {
                    answerButton[x].color = Color.white;
                }

                QuestionGenerate.questGenerate.GenerateQuestion();

                generating = false;
               
            }
        }

        highscoreSecond.text = PlayerPrefs.GetInt("HI").ToString("0");
       
    }

    public void ShowCorrectAnswer()
    {

        float randomMoney = Random.Range(0.01f, 0.5f);
        int randomIm;

        if (CriticalCoin >= prices[2] && !useSkills[2])
        {
            buttons[QuestionGenerate.correctID].color = showCorrectColor;
            buttons[QuestionGenerate.correctID].GetComponent<Animation>().Play("Great");

            do
            {
                randomIm = _rnd.Next(0, 3);
            }
            while (randomIm == QuestionGenerate.correctID);
            buttons[randomIm].color = showCorrectColor;
            buttons[randomIm].GetComponent<Animation>().Play("Great");

            CriticalCoin -= (int)prices[2];
            CCText.text = CriticalCoin.ToString("0");

            int level10 = Level * 10;
            prices[2] += (int)(CriticalCoin * randomMoney) + (int)Random.Range(0, level10);
            arak[2].text = prices[2].ToString("CC 0");

            useSkills[2] = true;
            skills[2].color = fail;

            StartCoroutine(boolSetTrue(2, skills[2]));

        }
    }
    public void Lose()
    {

        AdManager.Instance.ShowAd();
        RewardAds();
        Debug.Log("LOSE!");
       

       gameOverPanel.SetActive(true);

       Text scoreCounter = gameOverPanel.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>();
       scoreCounter.text = string.Format("- {0} -", Level);
       System.GC.Collect();
    }
    public void StartGame(GameObject obj)
    {
        System.GC.Collect();
        AN.SetActive(true);
        levelText.text = Level.ToString("0");
        CCText.text = CriticalCoin.ToString("0");

        Anim();
        isPlay = true;
        obj.GetComponent<Animation>().Play("P_out");
        TimeSec = 15;
        for (int x = 0; x < mainMenuObj.Length; x++)
        {
            mainMenuObj[x].GetComponent<Animation>().Play(mainMenuStrings[x]);
        }
        for (int x = 0; x < answerButton.Length; x++)
        {
  
                answerButton[x].color = Color.white;
            
        }
        Debug.Log("StartGame!");
        QuestionGenerate.questGenerate.GenerateQuestion();
    }
    public void Anim()
    {
        for (int x = 0; x < LoseObjects.Length; x++)
        {
            LoseObjects[x].GetComponent<Animation>().Play(OnAnimations[x]);
        }
    }

    public void OnApplicationQuit()
    {
        if (CriticalCoin >= 0)
        {
            PlayerPrefs.SetInt("CC", CriticalCoin);
        }
        else
        {
            PlayerPrefs.SetInt("CC", 0);
        }

        if (Level > PlayerPrefs.GetInt("HI"))
        {
            PlayerPrefs.SetInt("HI", Level);
        }
    }
    public void OpenLink(string linkUrl)
    {
        Application.OpenURL(linkUrl);
    }

    public IEnumerator boolSetTrue(int id,Image im)
    {
        yield return new WaitForSeconds(10);
        useSkills[id] = false;
        im.color = Color.white;
    }

    private void SetNull()
    {
        for (int x = 0; x < skills.Length; x++)
            skills[x].color = Color.white;

        for (int x = 0; x < useSkills.Length; x++)
            useSkills[x] = false;

        for (int x = 0; x < prices.Length; x++)
            prices[x] = 10;

        for (int x = 0; x < arak.Length; x++)
            arak[x].text = prices[x].ToString("CC 0");
    }

    public void NewQuest()
    {
        float randomMoney = Random.Range(0.01f, 0.5f);
        float money = (CriticalCoin * randomMoney) + Random.Range(0, 300);

        if (CriticalCoin >= prices[1] && !useSkills[1])
        {
            QuestionGenerate.questGenerate.GenerateQuestion();
            TimeSec = 15;
            CriticalCoin -= prices[1];
            CCText.text = CriticalCoin.ToString("0");

            int level10 = Level * 10;
            prices[1] += (int)(CriticalCoin * randomMoney) + (int)Random.Range(0,level10);

            arak[1].text = prices[1].ToString("CC 0");
            useSkills[1] = true;
            skills[1].color = fail;
            StartCoroutine(boolSetTrue(1, skills[1]));

        }
    }
    public void ReloadScene()
    {
        if (Level > PlayerPrefs.GetInt("HI"))
        {
            PlayerPrefs.SetInt("HI", Level);
            Social.ReportScore(Level, "CgkIoqn2mJsaEAIQAg", (bool success) => {

            });
        }

            if (Level > PlayerPrefs.GetInt("HI"))
            {
                PlayerPrefs.SetInt("HI", Level);
            }

            if (CriticalCoin >= 0)
            {
                PlayerPrefs.SetInt("CC", CriticalCoin);
            }
            else
            {
                PlayerPrefs.SetInt("CC", 0);
            }

        System.GC.Collect();
        Application.LoadLevel(0);
        Level = 0;
    }
    public void AddSec()
    {
        float randomMoney = Random.Range(0.01f, 0.5f);
        float money = (CriticalCoin * randomMoney) + Random.Range(0, 300);

        if (CriticalCoin >= prices[0] && !useSkills[0])
        {
            TimeSec += 10;
            CriticalCoin -= prices[0];
            CCText.text = CriticalCoin.ToString("0");
            int level10 = Level * 10;
            prices[0] += (int)(CriticalCoin * randomMoney) + (int)Random.Range(0, level10);

            arak[0].text = prices[0].ToString("CC 0");
            useSkills[0] = true;
            skills[0].color = fail;
            StartCoroutine(boolSetTrue(0,skills[0]));
        }
    }

    public void ClickContinue()
    {
        AdManager.Instance.ShowRewardedAd();
    }
    public void ContinueGame()
    {
        gameOverPanel.SetActive(false);
        QuestionGenerate.questGenerate.GenerateQuestion();

        for (int x = 0; x < answerButton.Length; x++)
        {
            answerButton[x].color = Color.white;
        }

        TimeSec = 15;
        isPlay = true;
    }
    public void RewardAds()
    {
        int r = Random.Range(1, 5);

        if(r < 3)
        {
            continuePanel.SetActive(true);
        }
        else
        {
            continuePanel.SetActive(false);
        }
    }
}
