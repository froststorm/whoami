﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class Question
{
    public string QuestionString;
    public string CorrectAnswer;

    public string[] failAnswers = new string[3];

    public Question(string quest, string corrAnswer, string[] fails)
    {
        quest = QuestionString;
        corrAnswer = CorrectAnswer;
        fails = failAnswers;
    }

}

[System.Serializable]
public class QuestionGenerate : MonoBehaviour
{
    public List<Question> questions = new List<Question>();

    [Header("Text Objects")]
    public Text questionText;
    public Text[] answers;

    public static int correctID;
    public int lastID;

    public static QuestionGenerate questGenerate;
    private System.Random _rnd = new System.Random();

    public List<int> lastIDs = new List<int>();
    public bool generateQuestion = false;
    public Text title;
    bool log;

    public Question question;

    [Header("Question Adder")]
    public Question adderQuestion;

    void Awake()
    { 
        questGenerate = this;

        //GenerateQuestion();
    }

    public bool existItem(List<int> list, int id)
    {
        System.GC.Collect();

        for (int x = 0; x < list.Count; x++)
        {
            if(list[x] == id)
            {
                return true;
            }
        }

        return false;

        System.GC.Collect();
    }
  
    public void GenerateQuestion()
    {

        System.GC.Collect();
        int randomQuestion = _rnd.Next(0,questions.Count);

        do
        {
            randomQuestion = _rnd.Next(0, questions.Count);
        }
        while (existItem(lastIDs, randomQuestion) == true);
        lastIDs.Add(randomQuestion);
        GameLogic.questionID = randomQuestion;

        correctID = _rnd.Next(0, answers.Length);

        Question quest = questions[randomQuestion];
        question = quest;
        questionText.text = quest.QuestionString;
        answers[correctID].text = quest.CorrectAnswer;

        List<Text> failsText = new List<Text>();

        for (int y = 0; y < answers.Length; y++)
        {
            if (y != correctID)
            {
               
                failsText.Add(answers[y]);
                Debug.Log("FailAnswersText: " + answers[y].name);
            }
        }

        for(int x = 0; x < quest.failAnswers.Length; x++)
        {
            failsText[x].text = quest.failAnswers[x];
        }
        System.GC.Collect();
    }
}
