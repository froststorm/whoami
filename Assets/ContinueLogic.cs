﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueLogic : MonoBehaviour {

    public string[] tipps;
    public Text tippText;
    public Text counterText;

    public float time;

    void Start()
    {
        time = 5;
        System.Random r = new System.Random();

        tippText.text = tipps[r.Next(0,tipps.Length)];
    }
    public void Update()
    {
        time -= Time.deltaTime;

        if(time <= 0)
        {
            GameLogic.Instance.ContinueGame();
            AdManager.Instance.counterPanel.SetActive(false);
        }
        else
        {
            counterText.text = string.Format("- {0} -",time.ToString("0"));
        }
    }
}
