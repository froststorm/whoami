// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class whoiamnew
{
        public const string achievement_neil_amstrong = "CgkIoqn2mJsaEAIQAQ"; // <GPGSID>
        public const string achievement_csiga = "CgkIoqn2mJsaEAIQBQ"; // <GPGSID>
        public const string achievement_ksz_einstein_vagy = "CgkIoqn2mJsaEAIQBg"; // <GPGSID>
        public const string leaderboard_iq_kirlyok = "CgkIoqn2mJsaEAIQAg"; // <GPGSID>
        public const string achievement_agathe_christie = "CgkIoqn2mJsaEAIQBA"; // <GPGSID>
        public const string achievement_a_villmkez = "CgkIoqn2mJsaEAIQAw"; // <GPGSID>

}

