﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroy : MonoBehaviour {

    public void ObjectActive(GameObject go, bool val)
    {
        go.SetActive(val);
    }
}
